local T = LibStub("WAR-AceLocale-3.0"):NewLocale("BlackBox", "frFR")

if T then

-- Actual text displayed in the black box
T["Respawning..."]      = L"Réapparition%.%.%."

-- Text displayed on the new 'bar'
T["Respawning in "]     = L"Réapparition "

end