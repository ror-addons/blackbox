local T = LibStub("WAR-AceLocale-3.0"):NewLocale("BlackBox", "itIT")

if T then

-- Actual text displayed in the black box
T["Respawning..."]      = L"Ritorno in gioco%.%.%."

-- Text displayed on the new 'bar'
T["Respawning in "]     = L"Ritorno in gioco tra "

end