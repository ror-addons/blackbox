<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="BlackBox" version="1.10" date="16/10/2009" >

		<Author name="Aiiane" email="aiiane@aiiane.net" />
		<Description text="Hides the infamous black box that sits in the middle of the screen while waiting to respawn after releasing." />
        
        <VersionSettings gameVersion="1.3.2" windowsVersion="1.0" savedVariablesVersion="1.0" />
        
        <Dependencies>
            <Dependency name="EA_GuildWindow" />
        </Dependencies>
        
		<Files>
            <File name="LibStub.lua" />
            <File name="AceLocale-3.0.lua" />
            
            <File name="Localization-enUS.lua" />
            <File name="Localization-esES.lua" />
            <File name="Localization-frFR.lua" />
            <File name="Localization-deDE.lua" />
            <File name="Localization-ruRU.lua" />
            <File name="Localization-itIT.lua" />
            
            <File name="BlackBox.xml" />
			<File name="BlackBox.lua" />
		</Files>
		
		<OnInitialize/>
		<OnUpdate>
            <CallFunction name="BlackBox.UpdateTimer" />
        </OnUpdate>
		<OnShutdown/>
		
        <WARInfo>
    <Categories>
        <Category name="COMBAT" />
        <Category name="OTHER" />
    </Categories>
    <Careers>
        <Career name="BLACKGUARD" />
        <Career name="WITCH_ELF" />
        <Career name="DISCIPLE" />
        <Career name="SORCERER" />
        <Career name="IRON_BREAKER" />
        <Career name="SLAYER" />
        <Career name="RUNE_PRIEST" />
        <Career name="ENGINEER" />
        <Career name="BLACK_ORC" />
        <Career name="CHOPPA" />
        <Career name="SHAMAN" />
        <Career name="SQUIG_HERDER" />
        <Career name="WITCH_HUNTER" />
        <Career name="KNIGHT" />
        <Career name="BRIGHT_WIZARD" />
        <Career name="WARRIOR_PRIEST" />
        <Career name="CHOSEN" />
        <Career name= "MARAUDER" />
        <Career name="ZEALOT" />
        <Career name="MAGUS" />
        <Career name="SWORDMASTER" />
        <Career name="SHADOW_WARRIOR" />
        <Career name="WHITE_LION" />
        <Career name="ARCHMAGE" />
    </Careers>
</WARInfo>

        
	</UiMod>
</ModuleFile>
