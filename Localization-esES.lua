local T = LibStub("WAR-AceLocale-3.0"):NewLocale("BlackBox", "esES")

if T then

-- Actual text displayed in the black box
T["Respawning..."]      = L"Reaparecer%.%.%."

-- Text displayed on the new 'bar'
T["Respawning in "]     = L"Reaparecer en "

end