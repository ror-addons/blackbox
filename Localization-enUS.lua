local T = LibStub("WAR-AceLocale-3.0"):NewLocale("BlackBox", "enUS", true)

if T then

-- Actual text displayed in the black box
T["Respawning..."]      = L"Respawning%.%.%."

-- Text displayed on the new 'bar'
T["Respawning in "]     = L"Respawning in "

end