BlackBox = {}
local firstLoad = true

local respawnTimeLeft = 0

local T = LibStub("WAR-AceLocale-3.0"):GetLocale("BlackBox")

function BlackBox.OnApplicationTwoButtonDialogHook(...)
    -- only pass down the call chain if it's not a respawn box
    if not (SystemData.Dialogs.AppDlg.text):find(T["Respawning..."]) then
        BlackBox.oldOnApplicationTwoButtonDialog(...)
    else
        -- show respawn timer window
        respawnTimeLeft = SystemData.Dialogs.AppDlg.timer
        StatusBarSetMaximumValue("RespawnTimerWindowBar", respawnTimeLeft)
        BlackBox.UpdateTimer(0)
        WindowSetShowing("RespawnTimerWindow", true)
    end
end

function BlackBox.UpdateTimer(timePassed)
    if respawnTimeLeft <= 0 then return end

    respawnTimeLeft = respawnTimeLeft - timePassed
    
    if respawnTimeLeft <= 0 then
        WindowSetShowing("RespawnTimerWindow", false)
    else
        StatusBarSetCurrentValue("RespawnTimerWindowBar", respawnTimeLeft)
        LabelSetText("RespawnTimerWindowText", T["Respawning in "]..towstring(math.ceil(respawnTimeLeft))..L"s...")
    end
end

function BlackBox.HealthUpdate()
    if respawnTimeLeft <= 0 then return end
    
    if GameData.Player.hitPoints.current > 0 then
        WindowSetShowing("RespawnTimerWindow", false)
        respawnTimeLeft = 0
    end
end

function BlackBox.SetHook()
    if not firstLoad then return end
    firstLoad = false
    
    BlackBox.oldOnApplicationTwoButtonDialog = DialogManager.OnApplicationTwoButtonDialog
    DialogManager.OnApplicationTwoButtonDialog = BlackBox.OnApplicationTwoButtonDialogHook
    
    CreateWindow("RespawnTimerWindow", false)
    
    d("BlackBox loaded.")
end

RegisterEventHandler(SystemData.Events.LOADING_END, "BlackBox.SetHook")
RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "BlackBox.SetHook")
RegisterEventHandler(SystemData.Events.PLAYER_CUR_HIT_POINTS_UPDATED, "BlackBox.HealthUpdate")